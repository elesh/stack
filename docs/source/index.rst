.. Elesh documentation master file, created by
sphinx-quickstart on Fri Apr 26 01:59:01 2019.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.

Welcome to Elesh's documentation!
=================================

Guide
^^^^^

.. toctree::
   :maxdepth: 3

   Getting Started
   API
   Stack
   Geospatial Search
   Logs Handler
   Daemon
   Redis
   Clickhouse



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
