# Getting started
The quick setup of the stack

## Set up Elesh
1. Install [Docker](https://www.docker.com/) Community Edition
2. Install [git](https://git-scm.com/)
3. [Clone project](https://git-scm.com/docs/git-clone) - [Project Link](https://gitlab.com/elesh/stack)
4. Set up all required databases for you with init-db.sh file that can be found in the /clickhouse/ folder. To gain more insight
on how to set up the table properly check [Clickhouse documentation](https://clickhouse.yandex/docs/en/) 
5. Run project in docker-environment - `docker-compose -f docker-compose.prod.yml up`, wait when project starts
([see more about docker-compose](https://docs.docker.com/compose/))

After those steps your Elesh is ready to accept any data.

## Send data to Elesh
Now you can set up the logger in your application.

You will need to send TCP JSON-like encoded strings to the IP (one of yours networks interfaces, or the server on which you run Elesh)
and Port (which is 1234, but can be changed in the docker-compose file).

If you are using Django (or any other application on Python) you can check out our [example application](https://gitlab.com/elesh/example-backend)
to see how it can handle logs.
```python

LOGGING = {
...
    'handlers': {
        'tcp': {
            'level': 'INFO',
            'class': 'logging_transport_json.JsonSocketHandler',
            'host': '159.65.113.20',
            'port': 1234,
        },
    },
    'loggers': {
...
            'handlers': ['tcp'],
            'level': 'INFO',
            }
...

```
It also uses a [package](https://pypi.org/manage/project/logging-transport-json/releases/), that has an instance of both UDP and TCP
Handlers that can send data of JSON-like string to the network. It was specifically created for this purpose and you are encouraged to use it
