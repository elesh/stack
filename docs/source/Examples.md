Examples
========

### Example of the json query to api
```json
{
  "statistics": 0,
  "query": "SELECT DayOfWeek, count(*) AS c FROM ontime WHERE Year == 1989 GROUP BY DayOfWeek ORDER BY c DESC;"
}
```

`statistics` - possible values: **0** or **1**. 0 - then statistics of query are not needed, 1 - then statistics are needed
`query` - query that need will be executed

### Example of json output with statistics
```json
{
    "0": {
        "col_name": [
            {
                "field": "DayOfWeek",
                "label": "DayOfWeek",
                "numeric": true,
                "sortable": true
            },
            {
                "field": "c",
                "label": "c",
                "numeric": true,
                "sortable": true
            }
        ],
        "response": [
            {
                "DayOfWeek": 2,
                "c": 735404
            },
            {
                "DayOfWeek": 3,
                "c": 735180
            },
            {
                "DayOfWeek": 1,
                "c": 733459
            },
            {
                "DayOfWeek": 5,
                "c": 731548
            },
            {
                "DayOfWeek": 4,
                "c": 731008
            },
            {
                "DayOfWeek": 7,
                "c": 706521
            },
            {
                "DayOfWeek": 6,
                "c": 668080
            }
        ],
        "statistics": {
            "bytes": 15305298,
            "elapsed_time": 168.13,
            "rows": 5101766,
            "total_rows": 5210112
        }
    }
}
```

### Example of json query with multiple SQL queries

**JSON query**
```json
{
	"statistics": 1,
	"query": "CREATE DATABASE IF NOT EXISTS logs;CREATE TABLE IF NOT EXISTS logs.django_logs (date Date,port Integer,level String,logsource String,pid Integer,program String,type String,host String,message String) Engine = Log;SELECT DayOfWeek, count(*) AS c FROM ontime WHERE Year == 1989 GROUP BY DayOfWeek ORDER BY c DESC;" 
}
```

**JSON response**
```json
{
    "0": {
        "col_name": [],
        "response": [],
        "statistics": {
            "elapsed_time": 113.935
        }
    },
    "1": {
        "col_name": [],
        "response": [],
        "statistics": {
            "elapsed_time": 4.904
        }
    },
    "2": {
        "col_name": [
            {
                "field": "DayOfWeek",
                "label": "DayOfWeek",
                "numeric": true,
                "sortable": true
            },
            {
                "field": "c",
                "label": "c",
                "numeric": true,
                "sortable": true
            }
        ],
        "response": [
            {
                "DayOfWeek": 2,
                "c": 735404
            },
            {
                "DayOfWeek": 3,
                "c": 735180
            },
            {
                "DayOfWeek": 1,
                "c": 733459
            },
            {
                "DayOfWeek": 5,
                "c": 731548
            },
            {
                "DayOfWeek": 4,
                "c": 731008
            },
            {
                "DayOfWeek": 7,
                "c": 706521
            },
            {
                "DayOfWeek": 6,
                "c": 668080
            }
        ],
        "statistics": {
            "bytes": 15305298,
            "elapsed_time": 28.801,
            "rows": 5101766,
            "total_rows": 5210112
        }
    }
}
```

### Query difficulty analyzer
Difficulty of the query is estimating using Halstead metrics:

For that we need to compute the following numbers given the query:

```
n1 = the number of distinct operators
n2 = the number of distinct operands
N2 = the total number of operands
Difficulty = n1/2 * N2/n2
```

Where operators are SQL operators and keywords and operands other words in query.