# Geospatial Search

Since our Log Storage contains data about movement of users while they are traveling in the taxi
it would be easier to compare those coordinates.

So we have chosen a function that checks if point is inside of the ellipse, so that we can make some queries related to the 
geographical locations (check statistics of users that are closer to some location)

## Function
The function thus get 6 values:

 - (x, y) - coordinates of the point on the plane
 - (x0, y0) - center of an ellipse on the plane
 - (a0, b0) - axes of an ellipse
 
The fact that the point is inside of an ellipse can be checked using simple inequality
 
 ```
(x-x0)^2/a0^2 + (y-y0)^2/b0^2 <= 1 
 ```