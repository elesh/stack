# ClickHouseManager

Class that handle user's queries

### Installation

```
git clone https://gitlab.com/elesh/backend.git
pip install -r requirements
```

### Initialization

```python
    ch_manager = ClickHouseManager(host=hostname)
```

### Methods

#### execute(query, settings={}, statistics=False)
___
**returns** - dictionary with query result and statstics (if flag was setted to True)


**query** - Query to Clickhouse database

**settings** - additional settings to the query (for exaples writing logs after each query )

**statistics** - add statistics of the query to the query result
___



#### halstead(query)
___
**returns** dictionary where *key* is Query and *value* is its difficulty

**query** Query to Clickhouse database 
***

