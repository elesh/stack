# Stack
App &rarr; Server &rarr; Redis &rarr; Daemon &rarr; Clickhouse  
All components of stack are easity replaceable, because abstract classes is used.  
```python
class TempStorage(ABC):
    def retrieve(self):
        pass

    def delete(self):
        pass

    def retrieve_and_delete(self):
        to_return = self.retrieve()
        self.delete()
        return to_return


class PersistentStorage(ABC):
    def store(self, data):
        pass
```
## Server  
This server is used to receive logs in JSON format from the app, via TCP. Then server push the log files to the Redis database.  
### Algoithm  
1. Establish the TCP connection between server and the app.  
2. Connect to Redis database.  
3. Create mutex instance in Redis database.  
Mutex is used to avoid pull, push conficts. For every app we create 2 lists in the Redis and Mutex for that. After that server starts pushing to the first list untill it won't be more than 1000 (because the insert to the Clickhouse is optimised for 1000 or more elements). After that the Mutex state is changed. After switch daemon.py pull the information from first list, while server is pushing to the second list, until it will be full and then again switch the mutex.
4. Receive one JSON from app.  
```json
{
  "name": "test.views",
  "msg": "New data",
  "args": null,
  "levelname": "INFO",
  "levelno": 20, "pathname":
  "/app/test/views.py",
  "filename": "views.py",
  "module": "views",
  "exc_info": null,
  "exc_text": null,
  "stack_info": null,
  "lineno": 13,
  "funcName": "index",
  "created": 1556241118.024052,
  "msecs": 24.051904678344727,
  "relativeCreated": 1469460.9005451202,
  "thread": 139907641759488,
  "threadName": "Thread-72747",
  "processName": "MainProcess",
  "process": 4213,
  "ip": "174.45.240.239",
  "user_id": 23,
  "order_id": 471,
  "car_id": 5,
  "longitude": 177.7904798467438,
  "latitude": 89.16339534311373
}
```
JSON example    
5. Push it to the list in Redis.  
6. Receive and push JSONs to the Redis list until it will have length equal or more than 1000.  
7. Switch the mutex.  
8. Push to the second list.  
9. Then it will be full switch mutex back and push again to the first list.  
## Daemon  
This part is used to pull data from Redis and insert it to the Clickhouse database.  
### Algorithm  
1. Connect to the Redis database.  
2. Connect to Clickhouse database.  
3. Check the size of Redis lists (depends on Mutex).  
4. If the size is less than 1000 sleep 10 seconds.  
5. If the size is more than 1000, pull a list from Redis.  
6. Parse the data and form Clickhouse INSERT query.  
7. Insert data to the Clickhouse database.
8. Delete the inserted data from Redis.
9. Restart from 3rd point.