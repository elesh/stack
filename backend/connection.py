from datetime import datetime
from clickhouse_driver import Client


class ClickHouseManager:

    # Creates a connection to database with settings
    def __init__(self, host):
        self.host = host
        self.client = None
        self.NUMERIC = ['UInt8', 'UInt16', 'UInt32', 'UInt64', 'Int8', 'Int16', 'Int32', 'Int64', 'Float32', 'Float64',
                        'Decimal']

    def create_connection(self):
        self.client = Client(host=self.host)

    def close_connection(self):
        self.client.disconnect()
        return "Connection has been closed"

    def execute(self, query, settings=dict(), statistics=False):

        # create additional settings to query execution if statistics flag is set to true
        if statistics:
            settings['log_queries'] = 1

        queries = query.split(";")
        response = {}
        for i, query in enumerate(queries):
            if query != "":
                query = query + ";"
                try:
                    start = datetime.now()
                    progress = self.client.execute_with_progress(
                        query, settings=settings, with_column_types=True
                    )
                    print(query)

                    rv = progress.get_result()
                    res = []
                    for row_answer in rv[0]:
                        dict_res = dict()
                        for j, row in enumerate(row_answer):
                            dict_res[rv[1][j][0]] = row
                        res.append(dict_res)

                    columns = []

                    for column in rv[1]:
                        config = {'field': column[0], 'label': column[0], 'sortable': True}
                        if column[1] in self.NUMERIC:
                            config['numeric'] = True
                        else:
                            config['numeric'] = False
                        columns.append(config)
                    if statistics:
                        try:
                            end = (datetime.now() - start).microseconds / 1000
                            stat = {
                                'rows': self.client.last_query.progress.rows,
                                'bytes': self.client.last_query.progress.bytes,
                                'total_rows': self.client.last_query.progress.total_rows,
                                'elapsed_time': end
                            }
                        except Exception:
                            end = (datetime.now() - start).microseconds / 1000
                            stat = {
                                'elapsed_time': end
                            }
                        response[i] = {'response': res, 'statistics': stat, 'col_name': columns}
                except Exception as e:
                    return {'exception': str(e)}
        return response
