#!/bin/bash
set -e

clickhouse client -n <<-EOSQL
    CREATE DATABASE IF NOT EXISTS default;
    CREATE TABLE IF NOT EXISTS django_logs (
      name String,
      msg String,
      args String,
      levelname String,
      levelno UInt32,
      pathname String,
      filename String,
      module String,
      exc_info String,
      exc_text String,
      stack_info String,
      lineno UInt32,
      funcName String,
      created Float64,
      msecs Float64,
      relativeCreated Float64,
      thread UInt64,
      threadName String,
      processName String,
      process UInt32,
      ip String,
      user_id UInt32,
      order_id UInt32,
      car_id UInt32,
      longitude Float64,
      latitude Float64
    ) Engine = Log;
EOSQL
