from storage import ClickHouseStorage, RedisStorage
import time

pers_stor = ClickHouseStorage()
temp_stor = RedisStorage()
while True:
    data = temp_stor.retrieve_and_delete()
    if data:
        pers_stor.store(data)
    else:
        time.sleep(10)

