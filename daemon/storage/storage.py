import json
import os
import sys
from abc import ABC
import logging
import redis
from clickhouse_driver import Client

buffer_size = 1000

clickhouse_host = os.environ.get("CLICKHOUSE_HOST")
redis_host = os.environ.get('REDIS_HOST')
redis_port = int(os.environ.get('REDIS_PORT'))
redis_db = int(os.environ.get('REDIS_DB'))
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()


class TempStorage(ABC):
    def retrieve(self):
        pass

    def delete(self):
        pass

    def retrieve_and_delete(self):
        to_return = self.retrieve()
        self.delete()
        return to_return


class PersistentStorage(ABC):
    def store(self, data):
        pass


class ClickHouseStorage(PersistentStorage):
    def __init__(self):
        self.cursor = Client(clickhouse_host)

    def store(self, data):
        logger.debug("Storing data in Clickhouse")
        keys = data[0].keys()
        sql_query = f"INSERT INTO django_logs ({','.join(keys)}) VALUES"
        logger.info(str(len(data)) + " was inserted!")
        logger.debug(sql_query)
        logger.debug(data)
        self.cursor.execute(sql_query, data)


class RedisStorage(TempStorage):
    def __init__(self):
        self.cursor = redis.StrictRedis(host=redis_host, port=redis_port, db=redis_db)

    def retrieve(self):
        logger.debug("Retrieving data in Redis")
        objs = []
        if self.cursor.get("Mutex").decode() == "True":
            if self.cursor.llen('buffer') >= buffer_size:
                objs = self.cursor.lrange('buffer', 0, -1)
        else:
            if self.cursor.llen('buffer0') >= buffer_size:
                objs = self.cursor.lrange('buffer0', 0, -1)
        filtered = []
        logger.debug(f"Got {len(objs)} keys")
        for el in objs:
            logger.debug(f"Element {el}")
            try:
                el = json.loads(el)
                for k in el.keys():
                    if not el.get(k, False):
                        el[k] = ''
                filtered.append(el)
            except json.decoder.JSONDecodeError:
                continue
        return filtered

    def delete(self):
        if self.cursor.get("Mutex").decode() == "True":
            if self.cursor.llen('buffer') >= buffer_size:
                self.cursor.delete('buffer')
                logger.debug(f"Deleting data in Redis in list 'buffer'")
        else:
            if self.cursor.llen('buffer0') >= buffer_size:
                self.cursor.delete('buffer0')
                logger.debug(f"Deleting data in Redis in list 'buffer0'")
