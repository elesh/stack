import logging
import os
import sys

import re
import socketserver
import redis

Buf_Size = 1000

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()


class HandlerTCPServer(socketserver.BaseRequestHandler):
    """
    The TCP Server class for demonstration.

    Note: We need to implement the Handle method to exchange data
    with TCP client.

    """

    def handle(self):
        # self.request - TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        logger.debug("{} sent:".format(self.client_address[0]))
        logger.debug(self.data.decode())
        d = re.split(r'(?<=[}{])\s*', self.data.decode().replace('}{', '} {'))
        if r.get("Mutex").decode() == "False":
            for data in d:
                r.rpush('buffer', data.encode())
            if r.llen('buffer') >= Buf_Size:
                r.set("Mutex", "True")
        else:
            for data in d:
                r.rpush('buffer0', data.encode())
            if r.llen('buffer0') >= Buf_Size:
                r.set("Mutex", "False")
        # just send back ACK for data arrival confirmation


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


HOST, PORT = "0.0.0.0", 9999

redis_host = os.environ.get('REDIS_HOST')
redis_port = int(os.environ.get('REDIS_PORT'))
redis_db = int(os.environ.get('REDIS_DB'))
r = redis.StrictRedis(host=redis_host, port=redis_port, db=redis_db)
r.set("Mutex", "False")
tcp_server = socketserver.TCPServer((HOST, PORT), HandlerTCPServer)

# Activate the TCP server.
# To abort the TCP server, press Ctrl-C.
tcp_server.serve_forever()
