const app = new Vue({
  el: '#editor',
  data() {
    return {
      query: `SELECT 
  FILTER(
    COUNT(*),
    WHERE \`f2-page-url\` LIKE '%st=%'),
  FILTER(
    COUNT(*),
    WHERE \`f2-page-url\` LIKE '%order_success=1%') 
  FROM Transaction 
  WHERE \`f2-page-url\` LIKE '%bestellung.html%' AND (\`user-agent\` LIKE '%Edge/12%') 
  SINCE 14 days AGO TIMESERIES` };

  },

  computed: {
    escapedQuery() {
      return this.query.
      replace(/&/g, "&amp;").
      replace(/</g, "&lt;").
      replace(/>/g, "&gt;").
      replace(/"/g, "&quot;").
      replace(/'/g, "&#039;");
    } },


  watch: {
    query() {
      this.highlightSyntax();
    } },


  mounted() {
    this.highlightSyntax();
  },

  methods: {
    highlightSyntax() {
      $('code').html(this.escapedQuery);

      $('.syntax-highlight').each(function (i, block) {
        hljs.highlightBlock(block);
      });
    },

    escapeHtml(unsafe) {
      return unsafe.
      replace(/&/g, "&amp;").
      replace(/</g, "&lt;").
      replace(/>/g, "&gt;").
      replace(/"/g, "&quot;").
      replace(/'/g, "&#039;");
    } } });