
import 'vuetify/dist/vuetify.min.css'
import Vue from "vue";
import router from "./router";
import VueCodemirror from 'vue-codemirror'
import 'codemirror/lib/codemirror.css'
import store from "./store";
import App from "./App";
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

Vue.use(Buefy);
import VueHighlightJS from 'vue-highlightjs';

// import { component as VueCodeHighlight } from 'vue-code-highlight';
import VueCodeHighlight from 'vue-code-highlight';
// import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'
import VuePrismEditor from "vue-prism-editor";
// import "vue-prism-editor/dist/VuePrismEditor.css"; // import the styles
// Vue.component("prism-editor", VuePrismEditor);

//
// Vue.use(Vuetify);
Vue.use(VueCodemirror);
// Vue.use(VuePrismEditor);
// Vue.use(VueHighlightJS);
// Vue.use(VueCodeHighlight);

new Vue({
  router,
  store,
  // VueCodeHighlight,
  // VueHighlightJS,
  render: h => h(App)
}).$mount('#app');


