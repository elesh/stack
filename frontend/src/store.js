import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    query: "SELECT * from logs.django_logs;",
    name: '',
    queries: []
  },
  mutations: {
    updateQuery(state, q){
      state.query = q

    },
    addQuery(state, q){
      state.queries.push({
        body: state.query,
        query: q
      })
    },
    editQuery(state, q){
      let queries = state.queries;
      queries.splice(queries.indexOf(q), 1);
      state.queries = queries;
      state.query = q.body;
      state.name = q.name


    }
  },
  actions: {

  },
  getters:{
        query: state => {return state.query},
        queries: state => {return state.queries}

  }
});


